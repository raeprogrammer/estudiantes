<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudiantes".
 *
 * @property int $id
 * @property string $nombre
 * @property int $edad
 * @property int $curso
 * @property string $imagen
 */
class Estudiante extends \yii\db\ActiveRecord
{
    public $archivo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudiantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'edad', 'curso'], 'required'],
            [['edad', 'curso'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['archivo'], 'file', 'extensions' => 'jpg,png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'curso' => 'Curso',
            'archivo' => 'Imagen',
        ];
    }
}
